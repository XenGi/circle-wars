Circle-Wars
===========

Circle-Wars is a simple game where 2 circles can attack each other.
You can only play this with 2 players, so find a friend and get fighting!

Requirements:
-------------

  * python >= 2.5
  * python-pygame >= 1.9

---

Circle Wars is free and open source software licensed under the MIT license.
