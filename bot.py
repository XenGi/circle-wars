# -*- coding: utf-8 -*-
"""
Circle-Wars is a simple game where 2 circles can attack each other.
You can only play this with 2 players, so find a friend and get fighting!

Created on Fri 16-11-2012_01:01:56+0100
@author: Ricardo (XenGi) Band <me@xengi.de>
"""


class Bot(object):
    def __init__(self, mode, mid):
        self.mode = mode
        self.mid = mid

    def get_attack(self, health, power, pixels):
        if self.mode == 0:
            if power == 10 and health > 20:
                return True
        if self.mode == 1:
            if power == 1:
                return True
        if self.mode == 2:
            pixels_near = 0
            pixels_far = 0
            for pixel in pixels:
                if pixel[0] and pixel[1] > self.mid:
                    pixels_near = pixels_near + pixel[2]
                else:
                    pixels_far = pixels_far + pixel[2]
            if pixels_far > health:
                return False
            if pixels_near < health:
                if power > 3:
                    return True
        return False
