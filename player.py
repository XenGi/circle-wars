# -*- coding: utf-8 -*-
"""
Circle-Wars is a simple game where 2 circles can attack each other.
You can only play this with 2 players, so find a friend and get fighting!

Created on Fri 16-11-2012_01:02:58+0100
@author: Ricardo (XenGi) Band <me@xengi.de>
"""

class Player():
    """
    The player
    """
    def __init__(self, pos):
        self.__health = 100
        self.__power = 0
        self.posx = pos

    def attack(self):
        """
        sends pixels to the enemy
        """
        ret = self.__power
        self.take_damage(self.__power)
        self.__power = 0
        return ret

    def increase_power(self):
        """
        get more power to attack
        """
        if self.__power < self.__health and self.__power < 10:
            self.__power = self.__power + 1

    def get_power(self):
        """
        returns the current power level
        """
        return self.__power

    def get_health(self):
        """
        returns the current power level
        """
        return self.__health

    def take_damage(self, points):
        if points < self.__health:
            self.__health = self.__health - points
            if self.__power > self.__health:
                self.__power = self.__health
            return False
        else:
            self.__health = 0
            return True
