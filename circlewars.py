#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Circle-Wars is a simple game where 2 circles can attack each other.
You can only play this with 2 players, so find a friend and get fighting!

Created on Fri 16-11-2012_01:01:56+0100
@author: Ricardo (XenGi) Band <me@xengi.de>
"""

import sys

import pygame
from pygame.locals import *

from player import Player
from bot import Bot


class Game(object):
    """
    The game
    """
    def __init__(self):
        pygame.init()
        self.fps_clock = pygame.time.Clock()
        #self.surface = pygame.display.set_mode((pygame.display.Info().current_w, pygame.display.Info().current_h), FULLSCREEN)
        self.surface = pygame.display.set_mode((800, 480), DOUBLEBUF)
        pygame.display.set_caption('circle-wars')
        self.font_obj = pygame.font.SysFont('Sans', 22, True)
        self.font_obj_big = pygame.font.SysFont('Sans', 48, True)

        self.col_red = pygame.Color(255, 0, 0)
        self.col_blue = pygame.Color(0, 0, 255)
        self.col_purple = pygame.Color(196, 0, 196)
        self.col_black = pygame.Color(0, 0, 0)
        self.col_white = pygame.Color(255, 255, 255)

        self.player1 = Player(128)
        self.player2 = Player(self.surface.get_width() - 128)
        self.player1_wins = False
        self.player2_wins = False

        # bots
        self.bot = Bot(2, self.surface.get_width() / 2)

        self.pixels = []
        self.time = 0

    def eventhandler(self):
        """
        change the direction of the snake according to pressed keys
        """
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.event.post(pygame.event.Event(QUIT))
            elif event.type == MOUSEBUTTONDOWN:
                if pygame.mouse.get_pos()[0] > self.surface.get_width() / 2:
                    if self.player2.get_power() > 0:
                        self.pixels.append([False, self.player2.posx - self.player2.get_health() / 2, self.player2.attack()])
                else:
                    if self.player1.get_power() > 0:
                        self.pixels.append([True, self.player1.posx + self.player1.get_health() / 2, self.player1.attack()])

    def run(self):
        """
        contains the gameloop
        """
        while True:
            self.eventhandler()

            if self.bot.get_attack(self.player2.get_health(), self.player2.get_power(), self.pixels):
                self.pixels.append([False, self.player2.posx - self.player2.get_health() / 2, self.player2.attack()])

            if self.time % 12 == 0:
                # give players POWER!
                self.player1.increase_power()
                self.player2.increase_power()
                # heal players
                if self.player1.get_health() < 100 and (self.player1.get_power() == 10 or self.player1.get_power() == self.player1.get_health()):
                    self.player1.take_damage(-1)
                if self.player2.get_health() < 100 and (self.player2.get_power() == 10 or self.player2.get_power() == self.player2.get_health()):
                    self.player2.take_damage(-1)

            # move pixels
            for i in range(len(self.pixels)):
                try:
                    if self.pixels[i][0]:
                        # player 1
                        self.pixels[i][1] = self.pixels[i][1] + 1
                    else:
                        # player 2
                        self.pixels[i][1] = self.pixels[i][1] - 1
                    if self.pixels[i][1] > self.player2.posx - self.player2.get_health() / 2 or self.pixels[i][1] < self.player1.posx + self.player1.get_health() / 2:
                        # pixel reached other player
                        if self.pixels[i][0]:
                            # pixel is owned by player 1
                            self.player1_wins = self.player2.take_damage(self.pixels[i][2])
                        else:
                            # pixel is owned by player 2
                            self.player2_wins = self.player1.take_damage(self.pixels[i][2])
                        self.pixels.pop(i)
                except IndexError, exc:
                    pass

            # drawing stuff
            self.surface.fill(self.col_black)

            # draw players
            pygame.draw.circle(self.surface,
                               self.col_red,
                               (128, self.surface.get_height() / 2),
                               self.player1.get_health() / 2,
                               0)
            pygame.draw.circle(self.surface,
                               self.col_blue,
                               (self.surface.get_width() - 128, self.surface.get_height() / 2),
                               self.player2.get_health() / 2,
                               0)

            # draw pixels
            for pixel in self.pixels:
                if pixel[0]:
                    # pixel is owned by player 1
                    pygame.draw.circle(self.surface, self.col_red, (pixel[1], self.surface.get_height() / 2), 1 + pixel[2], 0)
                else:
                    # pixel is owned by player 2
                    pygame.draw.circle(self.surface, self.col_blue, (pixel[1], self.surface.get_height() / 2), 1 + pixel[2], 0)

            # draw health
            health1 = self.player1.get_health()
            h1_surface = self.font_obj.render(str(health1), False, self.col_red)
            h1_rect = h1_surface.get_rect()
            h1_rect.midtop = (128, self.surface.get_height() / 2 - 128)
            self.surface.blit(h1_surface, h1_rect)

            health2 = self.player2.get_health()
            h2_surface = self.font_obj.render(str(health2), False, self.col_blue)
            h2_rect = h2_surface.get_rect()
            h2_rect.midtop = (self.surface.get_width() - 128, self.surface.get_height() / 2 - 128)
            self.surface.blit(h2_surface, h2_rect)

            # draw power
            power1 = self.player1.get_power()
            p1_surface = self.font_obj.render(str(power1), False, self.col_red)
            p1_rect = p1_surface.get_rect()
            p1_rect.midbottom = (128, self.surface.get_height() / 2 + 128)
            self.surface.blit(p1_surface, p1_rect)

            power2 = self.player2.get_power()
            p2_surface = self.font_obj.render(str(power2), False, self.col_blue)
            p2_rect = p2_surface.get_rect()
            p2_rect.midbottom = (self.surface.get_width() - 128, self.surface.get_height() / 2 + 128)
            self.surface.blit(p2_surface, p2_rect)

            # draw time
            time_surface = self.font_obj.render(str(self.time / 24), False, self.col_white)
            time_rect = time_surface.get_rect()
            time_rect.midtop = (self.surface.get_width() / 2, 0)
            self.surface.blit(time_surface, time_rect)

            pygame.display.update()
            pygame.display.flip()
            self.fps_clock.tick(24)

            self.time = self.time + 1

            if self.player1_wins or self.player2_wins:
                break

        blink = True
        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        pygame.event.post(pygame.event.Event(QUIT))

            # draw players
            # looser blinks because he's dying
            if self.player1_wins:
                win_surface = self.font_obj_big.render("RED WINS!", False, self.col_white)
                if blink:
                    pygame.draw.circle(self.surface, self.col_red, (128, self.surface.get_height() / 2), self.player1.get_health() / 2, 0)
                else:
                    pygame.draw.circle(self.surface, self.col_black, (128, self.surface.get_height() / 2), self.player1.get_health() / 2, 0)
            else:
                win_surface = self.font_obj_big.render("BLUE WINS!", False, self.col_white)
                if blink:
                    pygame.draw.circle(self.surface, self.col_blue, (self.surface.get_width() - 128, self.surface.get_height() / 2), self.player2.get_health() / 2, 0)
                else:
                    pygame.draw.circle(self.surface, self.col_black, (self.surface.get_width() - 128, self.surface.get_height() / 2), self.player2.get_health() / 2, 0)
            blink = not blink

            win_rect = win_surface.get_rect()
            win_rect.center = (self.surface.get_width() / 2, self.surface.get_height() / 2)
            self.surface.blit(win_surface, win_rect)

            pygame.display.update()
            pygame.display.flip()
            self.fps_clock.tick(12)


    def reset(self):
        """
        reset game
        """
        pass


if __name__ == '__main__':
    game = Game()
    game.run()
